package controller;

import java.util.ArrayList;

import model.data_structures.NumbersBag;
import model.logic.NumbersBagOperations;

public class Controller {

	private static NumbersBagOperations model = new NumbersBagOperations();
	
	
	public static NumbersBag createBag(ArrayList values){
         return new NumbersBag(values);		
	}
	
	
	public static double getMean(NumbersBag bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(NumbersBag bag){
		return model.getMax(bag);
	}
	public static double getMin(NumbersBag bag){
		return model.getMin(bag);
	}
	public static double getTotal(NumbersBag bag){
		return model.getTotal(bag);
	}
	public static double getFactor(NumbersBag bag){
		return model.getFactor(bag);
	}
}
