package model.logic;

import java.util.Iterator;

import com.sun.org.apache.xpath.internal.operations.Number;

import model.data_structures.NumbersBag;

public class NumbersBagOperations {



	public double computeMean(NumbersBag bag){
		int mean = 0;
		int length = 0;
		if(bag != null){
			
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean +=  iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(NumbersBag bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator <Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}

		}
		return max;
	}
	public int getMin(NumbersBag bag){
		int min = Integer.MAX_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( min > value){
					min = value;
				}
			}

		}
		return min;
	}
	public int getTotal(NumbersBag bag){
		int total = 0;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				total = total + value;
			}

		}
		return total;
	}
	public int getFactor(NumbersBag bag){
		int factor = 0;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();

				factor = factor * value;

			}

		}
		return factor;
	}
}
